from environs import Env
from flask import Flask, request, render_template
import requests
import psycopg2
import os
from question import question
from environs import Env

ROOT_PATH = os.getcwd()
ENV_PATH = os.path.join(ROOT_PATH, ".env")
Env.read_env(ENV_PATH, recurse=False, override=True)

app = Flask(__name__)

def fetch_product_info(merchant_id, product_id):
    url = f'http://shoplytics-api3.shoplinestg.com/api/v1/product_info?merchant_id={merchant_id}&product_id={product_id}'
    res = requests.get(url).json()
    return res['results'][0]

@app.route('/hello')
def hello():
    return "hello"

@app.route('/smart_recommendation_demo')
def smart_recommendation_demo():
    merchant_id = request.args.get('merchant_id')
    product_id = request.args.get('product_id')

    url = f'http://shoplytics-api3.shoplinestg.com/api/v1/product_recommendation?merchant_id={merchant_id}&product_id={product_id}'
    res = requests.get(url).json()
    target_product = fetch_product_info(merchant_id, product_id)
    recommendation_list = {product: fetch_product_info(merchant_id, product) for product in res['results'][:4]}

    return render_template(
        'demo.html', merchant_id=merchant_id,
        target_product=target_product, recommendation_list=recommendation_list
    )

@app.route('/smart_recommendation_test/<page>', methods=['GET', 'POST'])
def smart_recommendation_test(page='1'):
    if request.method == 'POST':
        sql = """
        INSERT INTO product_recommendation_verify (merchant_id, target_product, recommendation_product, score)
        VALUES(%s, %s, %s, %s)
        """
        merchant_id = question[str(int(page) - 1)]['merchant_id']
        target_product = question[str(int(page) - 1)]['product_id']
        conn = psycopg2.connect(
            host=os.getenv('DATABASE_HOST'),
            dbname=os.getenv('DATABASE_NAME'),
            user=os.getenv('DATABASE_USERNAME'),
            password=os.getenv('DATABASE_PASSWORD'),
            port=os.getenv('DATABASE_PORT'),
        )
        with conn.cursor() as cursor:
            for recommendation_product, score in request.form.to_dict(flat=True).items():
                cursor.execute(sql, (merchant_id, target_product, recommendation_product, score))
            conn.commit()

    if int(page) > 20:
        return 'End of testing.'
    else:
        merchant_id = question[page]['merchant_id']
        product_id = question[page]['product_id']

        url = f'http://shoplytics-api3.shoplinestg.com/api/v1/product_recommendation?merchant_id={merchant_id}&product_id={product_id}'
        res = requests.get(url).json()
        target_product = fetch_product_info(merchant_id, product_id)
        recommendation_list = {product: fetch_product_info(merchant_id, product) for product in res['results'][:4]}

        return render_template(
            'test.html', page=int(page), merchant_id=merchant_id,
            target_product=target_product, recommendation_list=recommendation_list
        )


if __name__ == '__main__':
    app.run(host='0.0.0.0')
